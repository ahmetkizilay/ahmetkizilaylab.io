importScripts('/cache-polyfill.js');

let CACHE_SITE = 'ahmetkizilay-com-site-cache-v2';
let CACHE_POSTS = 'ahmetkizilay-com-posts-cache-v1';

let urlsToDownload = {}
urlsToDownload[CACHE_SITE] = [
  '/',
  '/index.html',
  '/css/styles.css',
  '/img/avatar.jpeg'
];
urlsToDownload[CACHE_POSTS] = [];

let CACHE_NAMES = [CACHE_SITE, CACHE_POSTS];

self.addEventListener('install', function (e) {

  e.waitUntil(
    Promise.all(
      CACHE_NAMES.map(function (cache_name) {
        return caches.open(cache_name).then(function (cache) {
          return cache.addAll(urlsToDownload[cache_name]);
        });
      })
    )
  );
});

self.addEventListener('fetch', function (e) {
  var requestURL = new URL(e.request.url);
  if (requestURL.pathname === '/' || requestURL.pathname === '/index.html') {
    e.respondWith(
      caches.open(CACHE_SITE).then(function (cache) {
        return fetch(e.request).then(function (networkResponse) {
          cache.put(e.request, networkResponse.clone());
          return networkResponse;
        }).catch(function (err) {
          return caches.match(e.request);
        });
      })
    );
    return;
  }

  e.respondWith(
    caches.match(e.request).then(function (response) {
      return response || fetch(e.request);
    })
  );
});

self.addEventListener('activate', function (e) {
  e.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.map(function (cacheName) {
          if (CACHE_NAMES.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
})