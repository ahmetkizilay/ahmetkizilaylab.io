FROM ruby:2.1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/

RUN bundle install

COPY . /usr/src/app/

EXPOSE 4000

CMD ["jekyll", "serve", "--host=0.0.0.0"]
