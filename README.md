# ahmetkizilay.com

This is my personal website.
It is a Jekyll website, continuously built and deployed on Gitlab.com, and hosted on AWS S3.

To see the site in action: [https://ahmetkizilay.dom](https://ahmetkizilay.com)

#### Developing
Use `docker-compose` to develop and preview site if you don't want to bother with installing dependencies on your machine.
```
docker-compose up --build
# visit 127.0.0.1:4000
```
otherwise:
```
gem install bundler # if you don't have bundler
bundle install
jekyll serve
```

#### Building
The site is build with GitLab shared runners with every push to the master.
See `.gitlab-ci.yml` and `./script/cibuild` files to see how it works.

#### Deploying
the project is deployed to S3 to be distributed by CloudFront.
Gitlab CI takes care of deploying the generated site to S3.
See `.gitlab-ci.yml` and `./script/cideploy` file to see how it works.
