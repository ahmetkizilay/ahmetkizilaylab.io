---
layout: post
title: Setting up GitLab Runner For Continuous Integration
tags: gitlab, docker, continuous integration, tutorial,
excerpt: A tutorial on getting started with GitLab Runner for CI.
lang: en
---

### {{page.title}}

I wrote a tutorial on GitLab blog about how to get started with GitLab Runner.

In the post, I walk through a sample NodeJS application to configure a Gitlab
project to run tests with every push.

Read the article [here](https://about.gitlab.com/2016/03/01/gitlab-runner-with-docker/)
