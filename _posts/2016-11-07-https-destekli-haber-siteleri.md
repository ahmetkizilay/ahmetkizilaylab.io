---
layout: post
title: HTTPS destekli haber siteleri [TR]
tags: opinion, security, journalism, censorship, turkish
excerpt: Türkiye'nin sansüre ve gözetime elverişli sitelerini inceledik.
lang: tr
---

### {{page.title}}

Türkiye'deki haber sitelerinin HTTPS desteklerini inceleyip, HTTPS üzerinden yayın yapmanın hem daha güvenli hem de sansür mekanizmalarına karşı bir tepki olarak kullanabileceğine dair bir yazı hazırladım.

Yazının tamamını [Medium](https://medium.com/graph-commons/t%C3%BCrkiyenin-sans%C3%BCre-ve-g%C3%B6zetime-uygun-haber-siteleri-2e8a928401d3#.b9t2jnfz0)'da okuyabilirsiniz.